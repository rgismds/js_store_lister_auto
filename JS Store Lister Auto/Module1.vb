﻿Imports System.IO
Imports Microsoft.VisualBasic.FileIO
Imports Microsoft.Office.Interop.Excel
Imports System.Data.SqlClient


Module Module1
    Dim currDate As Date = DateTime.Now
    Dim sCsvFilesPath, sTempPath As String
    Dim dtFromDate, dtToDate As Date
    Dim sEmailTo, sEmailCC As String
    Dim lstFiles As List(Of String)
    Dim lookup As Dictionary(Of Integer, Date)


    Sub Main()

        sCsvFilesPath = "\\eu-fps\Client_Data\Sainsburys\Dashboard\DAILY_PCVAR"
        sTempPath = "c:\JS_Temp"
        If currDate.DayOfWeek = DayOfWeek.Monday Then
            dtFromDate = DateTime.Today.AddDays(-8)
        Else
            If currDate.DayOfWeek = DayOfWeek.Tuesday Then
                dtFromDate = DateTime.Today.AddDays(-9)
            End If
            If currDate.DayOfWeek = DayOfWeek.Wednesday Then
                dtFromDate = DateTime.Today.AddDays(-10)
            End If
            If currDate.DayOfWeek = DayOfWeek.Thursday Then
                dtFromDate = DateTime.Today.AddDays(-11)
            End If
            If currDate.DayOfWeek = DayOfWeek.Friday Then
                dtFromDate = DateTime.Today.AddDays(-11)
            End If
        End If
        dtToDate = dtFromDate.AddDays(6)
        PopulateStoreList()
        CreateStoreFile()


    End Sub


    Private Sub PopulateStoreList()
        Dim str As New List(Of String)
        Dim MyReader As StreamReader
        Dim nLine As Integer
        Dim sCurrLine As String
        Dim nStoreNumber As Integer
        Dim dtExportDate As Date


        'check temp folder exist if so delete it

        If Directory.Exists(sTempPath) Then
            My.Computer.FileSystem.DeleteDirectory(sTempPath,
                FileIO.DeleteDirectoryOption.DeleteAllContents)
        End If

        'Recreate the Temp Folder
        Directory.CreateDirectory(sTempPath)
        lookup = New Dictionary(Of Integer, Date)
        lstFiles = New List(Of String)
        Dim Folder As New IO.DirectoryInfo(sCsvFilesPath)
        For Each File As IO.FileInfo In Folder.GetFiles("*.csv", IO.SearchOption.TopDirectoryOnly) _
            .Where(Function(x) x.LastWriteTime >= (dtFromDate) AndAlso x.LastWriteTime <= (dtFromDate.AddDays(7)))
            'toolStatusText.Text = "Copying " & File.Name.ToString
            Dim CF As New IO.FileStream(File.FullName, IO.FileMode.Open)
            Dim CT As New IO.FileStream(Path.Combine(sTempPath, File.Name), IO.FileMode.Create)
            Dim len As Long = CF.Length - 1
            Dim buffer(1024) As Byte
            Dim byteCFead As Integer
            While CF.Position < len
                byteCFead = (CF.Read(buffer, 0, 1024))
                CT.Write(buffer, 0, byteCFead)
                'ProgressBar1.Value = CInt(CF.Position / len * 99)
                'System.Windows.Forms.Application.DoEvents()
            End While
            CT.Flush()
            CT.Close()
            CF.Close()
            ' FileCopy(File.FullName, Path.Combine(TempTextBox.Text, File.Name))
            lstFiles.Add(File.Name)
            'System.Windows.Forms.Application.DoEvents()
        Next

        If lstFiles.Count <> 7 Then
            ' MsgBox("File Count Wrong File's Found: " & lstFiles.Count)
        End If

        'ProgressBar1.Value = 0

        For Each oItem In lstFiles

            'ProgressBar1.Value = ProgressBar1.Value + (100 \ ListBox1.Items.Count)
            'toolStatusText.Text = "Reading File: " & oItem.ToString

            If My.Computer.FileSystem.GetFileInfo(sTempPath & "/" & oItem).Length > "155" Then
                Dim File_Date = Convert.ToDateTime(System.IO.File.GetLastWriteTime(sCsvFilesPath & "/" & oItem)).AddDays(-1).ToString(("dd/MM/yyyy"))
                nLine = 0
                Try
                    ' JR - 13/03/2017
                    ' Changed MyReader to be a StreamReader rather than a TextFieldParser as the TextFieldParser object
                    ' was generating parse errors on some lines that included double characters

                    MyReader = New StreamReader(sTempPath & "/" & oItem)
                    ' MyReader = New TextFieldParser(TempTextBox.Text & "/" & oItem.ToString)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

                Dim currentRow As String()
                ' Read the first line and do nothing with it
                If Not MyReader.EndOfStream Then
                    sCurrLine = MyReader.ReadLine()
                    currentRow = sCurrLine.Split(",")
                End If
                nLine += 1
                While Not MyReader.EndOfStream
                    Try
                        ' Read again the file
                        sCurrLine = MyReader.ReadLine()
                        currentRow = sCurrLine.Split(",")
                        'currentRow = MyReader.ReadFields()
                        Dim line_add = (currentRow(0) & "," & File_Date)
                        If Not str.Contains(line_add) Then
                            nStoreNumber = Convert.ToInt32(currentRow(0))
                            dtExportDate = File_Date
                            lookup.Add(nStoreNumber, dtExportDate)
                            'DataGridView1.Rows.Add(currentRow(0), File_Date)
                            str.Add(line_add)
                        End If
                        nLine += 1
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Line " & ex.Message & "is not valid and will be skipped.")
                    Catch ex As Exception
                        ex = ex
                    End Try
                End While
                MyReader.Close()
                MyReader.Dispose()

            End If

        Next

    End Sub

    Private Sub CreateStoreFile()

        Dim xlApp As Microsoft.Office.Interop.Excel.Application
        Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
        Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim range As Microsoft.Office.Interop.Excel.Range
        Dim sFile As String
        Dim nRow, nCol As Integer
        Dim sStoreType, sStoreName As String
        Dim sStoreListFolder As String
        Dim sDate As String

        Dim sOutputStoreFile As String
        Dim xoCmd As SqlCommand
        Dim sConnStr As String = ""
        Dim DBConn As SqlConnection

        'Cursor = Cursors.WaitCursor
        sStoreListFolder = "\\eu-fps.eu.ad.rgis.com\Private\Division\Schedules\Sainsbury's\Wkly Store Upload List\"
        sFile = sStoreListFolder + "Weekly_storelist_blank.xlsx"
        sOutputStoreFile = sStoreListFolder + "wc " + dtFromDate.Day.ToString("D2") + "." + dtFromDate.Month.ToString("D2") + "." + dtFromDate.Year.ToString("D2") + ".xlsx"

        xlApp = New Microsoft.Office.Interop.Excel.Application()
        xlWorkBook = xlApp.Workbooks.Open(sFile)
        xlWorkSheet = xlWorkBook.Worksheets(1)
        range = xlWorkSheet.UsedRange
        nRow = 2
        nCol = 1
        'Call LoadSavedConnectionDetailsFromRegistry(sConnStr)
        ' Database is fixed as Sainsburys at moment
        sConnStr = "Server = eu-db01\uksm;Database = Sainsburys;UID = Sainsburys;PWD = WZQ(sTy~#4%7Ua}+;Trusted_Connection = False"
        DBConn = New SqlConnection(sConnStr)
        DBConn.Open()
        xlWorkSheet.Columns(3).NumberFormat = "dd/MM/yyyy"
        For Each r In lookup
            If (Not IsNothing(r)) Then


                xoCmd = New SqlCommand
                xoCmd.Connection = DBConn
                xoCmd.CommandType = CommandType.StoredProcedure

                Try
                    ' Dim xdReportDate As Date
                    xoCmd.CommandText = "p_GetStoreNameAndType"
                    xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = r.Key
                    xoCmd.Parameters.Add("Store_Type", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                    xoCmd.Parameters.Add("Store_Name", SqlDbType.NVarChar, 255).Direction = ParameterDirection.Output
                    xoCmd.ExecuteNonQuery()

                    sStoreType = xoCmd.Parameters("Store_Type").Value.ToString
                    sStoreName = xoCmd.Parameters("Store_Name").Value.ToString
                    ' Format
                    xlWorkSheet.Cells(nRow, nCol) = sStoreType
                    ' Store Number
                    xlWorkSheet.Cells(nRow, nCol + 1) = r.Key.ToString()
                    ' Store Name
                    xlWorkSheet.Cells(nRow, nCol + 2) = sStoreName
                    ' Date
                    sDate = r.Value.ToString("dd/MM/yyyy")
                    xlWorkSheet.Cells(nRow, nCol + 3) = sDate

                Finally
                End Try

            End If
            nRow = nRow + 1
        Next
        xlApp.DisplayAlerts = False

        xlWorkBook.SaveAs(sOutputStoreFile)

        Dim cEmail As clsEmail
        Dim sEmailTo, sEmailCc, sBody, sSubject As String

        cEmail = New clsEmail()
        sBody = "Hello," + vbCrLf + vbCrLf
        sBody += "Please note we have determined there are " + lookup.Count.ToString() + " stores due to be imported for the week commencing "
        sBody += dtFromDate.Day.ToString("D2") + "." + dtFromDate.Month.ToString("D2") + "." + dtFromDate.Year.ToString("D2") + vbCrLf + vbCrLf
        sBody += "We shall wait for you to confirm this is correct before proceeding to import the data." + vbCrLf + vbCrLf
        sBody += "Regards" + vbCrLf + "Mapping & Data Services Team"
        sEmailTo = System.Configuration.ConfigurationManager.AppSettings.Get("EmailTo")
        sEmailCc = System.Configuration.ConfigurationManager.AppSettings.Get("EmailCc")
        'sEmailTo = "Robert.Holden@sainsburys.co.uk"
        'sEmailCc = "Magdalena.Malanowicz@sainsburys.co.uk;GRichards@rgis.com;VMolnar@RGIS.com;mds@rgis.com"
        sSubject = "Sainsburys import " + dtFromDate.Day.ToString("D2") + "." + dtFromDate.Month.ToString("D2") + "." + dtFromDate.Year.ToString("D2")
        ' If running in automatic mode then send store file immediately to Sainsburys
        cEmail.SendEmail(sEmailTo, sEmailCc, sSubject, sBody, sOutputStoreFile, "")


        xlWorkBook.Close()
        xlApp.Quit()
        DBConn.Close()
    End Sub
End Module
